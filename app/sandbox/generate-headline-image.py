# Name: Generate Headline Image
# Date: 2024-06-15 / JRO
# Description: Generates an image for the news headlines
#
# OUTCOME BACKLOG
# - Save the image
# - List of news headlines, each with an image
#
# ACCOMPLISHMENTS
#  - Generate an image from a prompt, using an API
#
# OpenAI: https://platform.openai.com/login?launch

import requests
from PIL import Image
from io import BytesIO
from datetime import datetime

from openai import OpenAI
client = OpenAI()


prompt = "news thumbnail for headline: DNA: The data companies, countries and cops all want | 60 Minutes Full Episodes."
prompt += "Make it hyper-realistic, cool textures, unique angle, cinematic angle, perfect lighting, perfect composition, light vignette, with a sesame street style muppet host news anchor, the muppet is slightly off"

# prompt = "best italian greyhound fail meme, cinematic extreme close-up, dramatic lighting, shallow depth-of-field, perfect composition, great textures, perfectly color graded, unique angle, cool camera effect"

print(f"Prompt: {prompt}")

response = client.images.generate(
  model="dall-e-3",
  prompt=prompt,
  size="1024x1024",
  quality="standard",
  n=1,
)

image_url = response.data[0].url

print(f"Generated image URL: {image_url}")


# Download the image from the URL
image_response = requests.get(image_url)
image_data = image_response.content


# Generate filename with current ISO datetime
now = datetime.now()
timestamp = now.isoformat(timespec='seconds')  # Format: YYYY-MM-DDTHH:MM:SS
image_filename = f'generated_image_{timestamp}.jpg'  # Adjust file extension as needed


# Save the image locally
image = Image.open(BytesIO(image_data))
image.save(image_filename)  
print(f"Image saved to: {image_filename}")