# Title: Generate Image Newsfeed
# Updated: 2024-06-16 / JRO
# 
# DEV BACKLOG
# - Check for existing images on S3 before generating a new one
# - Enable as Lambda
# - Added to CloudWatch / Lambda automation
# - Custom prompt prefix: ""
#
# DEV ICEBOX
# - Python script in a Docker container
# - Cron running in Docker container
# - Database (AWS RDS)


# Read RSS Feeds
import feedparser

# Store files on AWS
import boto3

# Connect to APIs
import requests

# Work with local files
import os
from datetime import datetime

# Slugify
import unicodedata
import re

# OpenAI API
from openai import OpenAI

prompt_prefix = '''
Original Prompt: Elon Musk has dropped his lawsuit against OpenAI and Sam Altman

Step 1: Rewrite the prompt so that it complies with your content policy. Get as close as you can to the original headline, but change it so that you can generate it without error. Replace any words that will generate errors with your content policy.

Step 2: Generate image for new prompt that complies with your content policy.
'''

# Local file name
page_filename = "_PUBLIC/story-image-tiles.html"

# SETTINGS

# Maximum articles to generate 
# (Useful for testing in development)
max_articles = 9

# AWS S3 setup
s3 = boto3.client('s3')
bucket_name = 'opentech.news-public'
cloudfront_url = 'https://opentech.news'

# From Django Source code
# Source: https://github.com/django/django/blob/fa7848146738a9fe1d415ee4808664e54739eeb7/django/utils/text.py#L452
def slugify(value, allow_unicode=False):
    """
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize("NFKC", value)
    else:
        value = (
            unicodedata.normalize("NFKD", value)
            .encode("ascii", "ignore")
            .decode("ascii")
        )
    value = re.sub(r"[^\w\s-]", "", value.lower())
    return re.sub(r"[-\s]+", "-", value).strip("-_")


# Function to generate image using DALL-E API
def generate_image(prompt):

    client = OpenAI()

    print(f"► Generating image for prompt: {prompt}")
    
    try:
        response = client.images.generate(
        model="dall-e-3",
        prompt=prompt,
        size="1024x1024",
        quality="standard",
        n=1,
        )
    except:
        print(f"ERROR: Could not generate image for prompt: {prompt}")
        return None
    
    image_url = response.data[0].url
    image_response = requests.get(image_url)
    if image_response.status_code == 200:
        return image_response.content
    
    return None

# Function to save image to S3
def save_image_to_s3(image_content, image_name):
    print(f"Saving image: {image_name}")
    s3.put_object(Bucket=bucket_name, Key=image_name, Body=image_content, ContentType='image/png')
    return f'{cloudfront_url}/{image_name}'

# Main Lambda handler
#def lambda_handler(event, context):
def main():
    rss_feed_url = "https://www.google.com/alerts/feeds/07480943305324684294/8607219941662562161"
    
    print("► Getting RSS feed")
    feed = feedparser.parse(rss_feed_url)
    
    client = OpenAI()
    
    html_content = '''
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Open Tech News</title>

        <!-- Bootstrap Styles -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="styles/image-feed.css" rel="stylesheet">
    </head>
    <body>
        <div id="content-container" class="container-fluid">

            <div id="header" class="container-fluid" >
                <h1>Open Tech News</h1>
            </div> <!-- /header-->

            <!-- Floating Button -->
            <script src="https://static.elfsight.com/platform/platform.js" data-use-service-core defer></script>
            <div class="elfsight-app-2f74152d-a39e-4841-bc40-d968690b9dea" data-elfsight-app-lazy></div>
        
        <div class="container">
        <div class="row"> <!-- TO-DO: Occur every 3 -->
    '''

    for i, entry in enumerate(feed.entries[:max_articles]):
        if i % 3 == 0:
            # Start a new row for every third article
            if i != 0:
                html_content += '</div><!-- /Row -->\n'
            html_content += '<div class="row">\n'




        title = entry.title
        link = entry.link
        image_name = f'images/{ slugify(title)}.png'
        
        # Generate image for the title
        image_content = generate_image(title)
        if image_content:
            image_url = save_image_to_s3(image_content, image_name)
            html_content += f'''
            <div class="col-sm">
                <a href="{link}">
                    <img src="{image_url}" width="300px">
                    <!-- TO-DO: Update in CSS -->
                    <div style="width: 300px"> 
                        <h2>{title}</h2>
                    </div>
                </a>
            </div> <!-- /col-sm -->
            '''
        else:
            # TO-DO: For stories with no images
            html_content += f'''
                <div class="col-sm">
                    <a href="{link}">
                        <img src="images/image-placeholder.png" width="300px">
                        <h2>{title}</h2>
                    </a>
                </div> <!-- /col-sm -->
                '''
    # Close the final row
    html_content += '</div><!-- /Row -->\n'
    
    html_content += '''
        </div><!-- /container -->
        </div> <!-- /content-container -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    </body>
    </html>
    '''
    
    # Save HTML to S3
    # TO-DO: Set filename in settings
    html_file_name = 'image-headlines.html'

    if __name__ == "__main__":
        print(f"Saving to local file: {page_filename}")
        with open(page_filename, 'w') as f:
            f.write(html_content)

    print("Saving file to cloud storage...")
    s3.put_object(Bucket=bucket_name, Key=html_file_name, Body=html_content, ContentType='text/html')
    return {
        'statusCode': 200,
        'body': 'RSS Feed with images updated successfully!'
    }

if __name__ == "__main__":
    main()