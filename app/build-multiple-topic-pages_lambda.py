# Title: Build Multiple Topic feeds
# Date: 2024-06-20 / JRO
# Description: Build multiple topic-focused newsfeed pages
#
# DEV BACKLOG
# - Create 1 topic HTML page (example: https://opentech.news/quantum-computing/)
#   - Local file
#   - Uploaded to cloud (S3)
# - Create 3 topic pages (examples: "/voice-synthesis", "/conversational-ai")
#   - Local file
#   - Uploaded to Cloud
# - Automated every minute
#
# IDEAS
# - Tech for good / positive social impact
# - ( PUT YOUR USER STORY HERE ) - $50/mo Landon!
#    - Articles --> Vector database --> take a word/phrase --> Similarity search
#   -  Example: Jaguar, Liger, Dolphin -> Search for Cat, retrieve similar "semantic meaning"


# IMPORTS
#----------------
# Read RSS Feeds
import feedparser

# Store files on AWS
import boto3

# Work with local files
import os

# Slugify
import unicodedata
import re


from datetime import datetime



# SETTINGS
#-----------
max_articles = 20
bucket_name = "opentech.news-public"
local_file_path = "_PUBLIC/topics/"
remote_file_path = "topics/"

# Setup AWS
s3 = boto3.client('s3')


# From Django Source code
# Source: https://github.com/django/django/blob/fa7848146738a9fe1d415ee4808664e54739eeb7/django/utils/text.py#L452
def slugify(value, allow_unicode=False):
    """
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize("NFKC", value)
    else:
        value = (
            unicodedata.normalize("NFKD", value)
            .encode("ascii", "ignore")
            .decode("ascii")
        )
    value = re.sub(r"[^\w\s-]", "", value.lower())
    return re.sub(r"[-\s]+", "-", value).strip("-_")


# Save to AWS S3
def save_to_s3(html_content, bucket_name, file_name):
    s3.put_object(
        Body=html_content, 
        Bucket=bucket_name, 
        Key=file_name,
        ContentType='text/html') # Important!



# TOPIC FEEDS - Title & RSS XML 
topic_feeds = [
    ('Conversational AI', 'https://www.google.com/alerts/feeds/07480943305324684294/10954025684966492260'),
    ('Maker Space', 'https://www.google.com/alerts/feeds/07480943305324684294/12335099446066770163')
]

# Builds multiple topic pages
def build_newsfeed_pages(topic_feeds):
    print("► Building topic feeds...")
    print(f"Topic feeds: {topic_feeds}")

    for topic_feed in topic_feeds:
        build_topic_feed_page(topic_feed)


# Build a topic page
def build_topic_feed_page(topic_feed):
    topic_feed_url = topic_feed[1]
    topic_title = topic_feed[0]

    print("\n► Building Topic Feed...")
    print(f"Topic feed: {topic_title}")
    
    # Slufigy: Turns "open source tech" to "open-source-tech"
    article_title_slug = slugify(topic_feed[0])

    topic_filename = f"{article_title_slug}.html"
    print(f"Slugified title: {article_title_slug}")


    # HTML HEADER
    html_content = f'''
        <html>
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>{topic_title}</title>
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
            <link href="../styles/multifeed.css" rel="stylesheet">
            

            <!-- OPEN GRAPH -->
            <meta name="description" content="Your real-time feed for the latest in open source technology...">
            <meta property="og:site_name" content="Open Tech News">
            <meta property="og:url" content="https://opentech.news/">
            <meta property="og:title" content="Open Tech News">
            <meta property="og:type" content="website">
            <meta property="og:description" content="Your real-time feed for the latest in open source technology...">
            <meta name="twitter:site" content="@">
            <meta name="twitter:card" content="summary_large_image">
            <meta name="twitter:title" content="Open Tech News">
            <meta name="twitter:description" content="Your real-time feed for the latest in open source technology...">
            <meta property="og:image" content="https://opentech.news" />
            <meta property="og:image:secure_url" content="https://opentech.news/images/opentechnews_banner.png" />
            <meta property="og:image:width" content="1200" />
            <meta property="og:image:height" content="628" />

        </head>
        <body>
        '''

    # Generate Navbar
    # TO-DO: Make dynamic
    html_content += '''
        <nav class="navbar">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/topics/conversational-ai.html">Conversational AI</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/topics/maker-space.html">Maker Space</a>
                </li>
            </ul>
            </div>
        </div>
        </nav>


        <!-- Floating button --> 
        <script src="https://static.elfsight.com/platform/platform.js" data-use-service-core defer></script>
        <div class="elfsight-app-2f74152d-a39e-4841-bc40-d968690b9dea" data-elfsight-app-lazy></div>

        '''

    html_content += f'''
        <!-- Content Container -->
        <div class="container">
        
        <h1>{topic_title} News</h1>
        
        <div class="list-group">
    '''

    feed = feedparser.parse(topic_feed[1])
    for article in feed.entries[:max_articles]:
        html_content += f'''
            <a href="{article.link}" class="list-group-item list-group-item-action">
                {article.title}
            </a>
        '''
        print(f"- Article: {article.title}")

    
    # HTML FOOTER
    html_content += '''
    </div> <!-- /list-group -->
    
    </div> <!-- /Content container -->

    <!-- Bootstrap Style Code -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>

    </body>
    </html>
    '''

    if 'AWS_LAMBDA_FUNCTION_NAME' in os.environ:        
        # Save HTML file on Cloud storage
        print("Saving to Cloud Storage...")
        save_to_s3(html_content, bucket_name, remote_file_path + topic_filename)
    else:
        # Save topic-feed HTML file locally
        with open(local_file_path + topic_filename, 'w') as f:
            f.write(html_content)
        print("Saving to Cloud Storage...")
        save_to_s3(html_content, bucket_name, remote_file_path + topic_filename)


# MAIN / RUN LOOP

if 'AWS_LAMBDA_FUNCTION_NAME' in os.environ:
    # When run in AWS Lambda
    def lambda_handler(event, context):
        build_newsfeed_pages(topic_feeds)
else:
    # Local
    build_newsfeed_pages(topic_feeds)
    