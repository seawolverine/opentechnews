import os
import boto3
import feedparser

from collections import OrderedDict

# MODELS
class Article:
    def __init__(self, url, title, publish_date=None, topics=None):
        self.url = url
        self.title = title
        self.publish_date = publish_date
        self.topics = []

    def __str__(self):
        return f"{self.title}\n"

class Topic:
    def __init__(self, title, articles=None):
        self.title = title
        self.articles = []

    def __str__(self):
        return f"Topic: {self.title}\n"

# AWS S3 Client
s3 = boto3.client('s3')

# Save HTML content as a file on AWS S3 bucket
def save_to_s3(html_content, bucket_name, file_name):
    s3.put_object(
        Body=html_content,
        Bucket=bucket_name,
        Key=file_name,
        ContentType='text/html'
    )

# NEWS FEED LINKS
news_feed_links = OrderedDict([
    ('Open-Source', 'https://www.google.com/alerts/feeds/07480943305324684294/8607219941662562161'),
    ('Renewable Energy', 'https://www.google.com/alerts/feeds/07480943305324684294/6721195691717171314'),
    ('Quantum Computing', 'https://www.google.com/alerts/feeds/07480943305324684294/13001886749545614150'),
    ('Robotics', 'https://www.google.com/alerts/feeds/07480943305324684294/2263818163143913699'),
    ('Edge Computing', 'https://www.google.com/alerts/feeds/07480943305324684294/18177243892928034657'),
    ('Artificial Intelligence', 'https://www.google.com/alerts/feeds/07480943305324684294/7966097943229416105'),
    ('Augmented Reality', 'https://www.google.com/alerts/feeds/07480943305324684294/7877834639065919698'),
    ('Blockchain', 'https://www.google.com/alerts/feeds/07480943305324684294/12763978978375485755'),
    ('5G Technology', 'https://www.google.com/alerts/feeds/07480943305324684294/16403034459037924628'),
    ('Internet of Things', 'https://www.google.com/alerts/feeds/07480943305324684294/9058242406592932187'),
    ('Biotechnology', 'https://www.google.com/alerts/feeds/07480943305324684294/53244181703901167')
])

# Main lambda function handler
def lambda_handler(event, context):
    topics = set()  # Topics

    print("▶ Downloading Articles from News feeds...")

    # Loop through News Feeds, and save the Articles
    for topic_name, rss_link in news_feed_links.items():
        new_topic = Topic(topic_name)
        feed = feedparser.parse(rss_link)
        print("--------")
        print(f"Topic: {topic_name} ({len(feed.entries)} articles)")

        # Loop over a Newsfeed's Articles, save them in the database
        for article in feed.entries:
            article_title = article.title
            article_link = article.link

            new_article = Article(url=article.link, title=article_title, topics=topic_name)
            print(f"-- Article: {new_article}")
            new_topic.articles.append(new_article)

        topics.add(new_topic)

    # Generate HTML output
    html_output = generate_html(topics)

    # Save HTML output file on Cloud Storage
    bucket_name = 'opentech.news-public'
    file_name = 'multi-feed.html'
    save_to_s3(html_output, bucket_name, file_name)

    return {
        'statusCode': 200,
        'body': 'Multi-topic newsfeed HTML generated and saved to S3.'
    }

def generate_html(topics):
    # Generate HTML content
    html_output = '''
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Open Tech News</title>

        <!-- Bootstrap Styles -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="styles/multifeed.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <div class="container-fluid">
                <h1>Open Tech News</h1>
            </div>
            <div class="elfsight-app-2f74152d-a39e-4841-bc40-d968690b9dea" data-elfsight-app-lazy></div>
        </div>
        <div class="container-fluid">
    '''

    # Generate Main Feed
    html_output += '''
        <div class="list-group">
    '''
    for topic in topics:
        if topic.title == main_feed_topic:
            if topic.articles:
                for article in topic.articles[:main_feed_article_limit]:
                    html_output += f'''
                    <a href="{article.url}" class="list-group-item">
                    {article.title}
                    </a>      
                    '''
    html_output += '''
        </div>
    '''

    # Generate Topic Feeds
    for topic in topics:
        if topic.title != main_feed_topic:
            if topic.articles:
                html_output += f'''
                <div class="list-group">
                    <h2>{topic.title}</h2>
                '''
                for article in topic.articles[:topic_feed_article_limit]:
                    html_output += f'''
                    <a href="{article.url}" class="list-group-item">
                    {article.title}
                    </a>
                    '''
                html_output += '''
                </div>
                '''

    # HTML Footer
    html_output += '''
        <div style="height: 50px;"></div>
        </div> <!-- /content container -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    </body>
    </html>
    '''

    return html_output
