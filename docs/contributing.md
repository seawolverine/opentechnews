---
title: "How to contribute"
date: 2024-06-15
---

# How to Contribute
Updated: 2024-06-15 / JRO


## Check out code
```
git clone  https://gitlab.com/opentechnews/opentech.news-website.git
```

## Configure Git User
git config --global user.name "opentech.news"
git config --global user.email "services@opentech.news"