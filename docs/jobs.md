---
title: "Jobbies"
date: 2024-06-16
---
# Jobbies

## Roles we're interested in
- Front-end web application developer:  "Make it pretty!"
    - Tools: HTML, CSS, Bootstrap, React
- Machine Learning / API developer: "Put the puzzles pieces together"
    - Tools: Python, OpenAI API, ChatGPT API, DALL-E API, Midjourney, Docker
- Voice synth: "Make it talk"
    - Tools: ElevenLabs, Piper
- Back-end web application developer: "Build the machine"
    - Tools: Python, Django, Docker, SQLite, Postgres / AWS RDS, testing
- Platforms: "Host the machine"
    - Tools: AWS S3, CloudFront, EC2 + Docker?; Render.com + Docker?
- Automation: "Automate the machine"
     - Tools: AWS CloudWatch, cron; GitLab CI/CD


## Tech Stack
- Web server: AWS Cloudfront
- Storage: AWS S3
- Feed generator: Python script running on AWS Lambda
- Automation: AWS CloudWatch runs the Lambda script every minute

### About the Feed Generator Script
- Feedparser connects to several Google News Alerts RSS feeds