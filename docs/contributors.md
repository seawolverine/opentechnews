---
title: "Open Tech News Contributors"
---

# Contribution Journal

## 2024-06-20
- Paul Zabelin - http://github.com/paulz
- Jonah Price - linkedin.com/in/JonahPrice / jonahsalberprice@gmail.com 541-554-6421
- George - https://gitlab.com/seawolverine
- Landon
- JRO
- Cliff
- Trace Wax



## 2024-06-18 Jam session
- http://github.com/paulz - Paul Zabelin
- https://www.varatenterprises.com - Jordan Varat
- https://jonathan.rogivue.net - JRO
- https://www.specialhawaiitours.com - Cliff 
