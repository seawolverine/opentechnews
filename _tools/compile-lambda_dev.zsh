# Title: Compile Lambda (DEV)
# Updated: 2024-06-16 / JRO
# 
# Description:
# - Copies python file to /release
# - Packages python dependencies into /release
# - Creates zip file for uploading to AWS


code_file="build-multiple-topic-pages_lambda.py"
#code_file="lambda_function.py"

# Get the current date and time in the desired format
build_path="_releases/$(date +"v%Y-%m-%d_%H%M")"

# Create the folder
mkdir $build_path

print "Copying python file to $build_path"

cp app/$code_file $build_path/lambda_function.py

pip freeze > $build_path/requirements.txt

pip install -r $build_path/requirements.txt --target $build_path

cd $build_path

zip lambda.zip -r ./*