# Description:
# - Copies python file to /release
# - Packages python dependencies into /release
# - Creates zip file for uploading to AWS


cp app/lambda_function.py _releases/
cd _releases/
pip install feedparser --target .
zip lambda.zip -r ./*